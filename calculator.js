var result = "";
var operator = "";
var clearFlag = false;

function numFunction(num) {
    var number;
    var originData = document.getElementById("answer").innerHTML;
    if (clearFlag === true) {
        originData = "";
        clearFlag = false;
    }
    number = "" + originData + num;
    document.getElementById("answer").innerHTML = number;
}

function resetFunction() {
    document.getElementById("answer").innerHTML = "";
    result = "";
    operator = "";
}

function mathsFunction(method) {
    currentNum = parseInt(document.getElementById("answer").innerHTML);
    operator = method;
    clearFlag = true;
    if (result != "") { 
        switch (method) {
            case "plus":
                result += currentNum;
                break;
            case "minus":
                result -= currentNum;
                break;
            case "times":
                result *= currentNum;
                break;
            case "divide":
                result /= currentNum;
                break;
            default:
                document.getElementById("answer").innerHTML = result;
        }
    } else {
        result = currentNum;
        currentNum = "";
    }
}

function getResult() {
    currentNum = parseInt(document.getElementById("answer").innerHTML);
    switch (operator) {
        case "plus":
            result += currentNum;
            break;
        case "minus":
            result -= currentNum;
            break;
        case "times":
            result = result * currentNum;
            break;
        case "divide":
            result /= currentNum;
            break;
        default:
            document.getElementById("answer").innerHTML = result;
    }
    document.getElementById("answer").innerHTML = result;
    clearFlag = true;
    operator = "";
    result = "";
}
